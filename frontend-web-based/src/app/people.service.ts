import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Person } from './person';

@Injectable()
export class PeopleService {

  	//private apiURL = 'http://test-frontend-neppo.herokuapp.com/pessoas';
    private apiURL = 'http://localhost:8080/person/';
    private jsonSufix = '.json';
    private options: any;

  	constructor(private http: Http) {
		let head = new Headers({ 'Content-Type': 'application/json' });
	    this.options = new RequestOptions({ headers: head });
  	}

    getPeople(): Observable<Person[]> {
    	return this.http.get(this.apiURL).map(res => { 
        	return res.json()._embedded.person.map(item => {
            	return item;
          	});
        });
    }

  	getPerson(personId): Observable<Person> {
    	return this.http.get(this.apiURL + personId).map(res => { 
        	return res.json();
        });
    }

  	createPerson(person) {
    	return this.http.post(this.apiURL, person, this.options);
  	}

  	updatePerson(person) {
     	return this.http.put(this.apiURL + person.id, person, this.options);
    }

  	deletePerson(personId) {
     	return this.http.delete(this.apiURL + personId);
    }

}