import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule }   from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { PeopleTableComponent } from './people-table/people-table.component';
import { PersonComponent } from './person/person.component';
import { NewPersonComponent } from './new-person/new-person.component';
import { PeopleComponent } from './people/people.component';
import { PeopleReportComponent } from './people-report/people-report.component';
import { GenderBarChartComponent } from './gender-bar-chart/gender-bar-chart.component';
import { AgeBarChartComponent } from './age-bar-chart/age-bar-chart.component';

import { ChartsModule } from 'ng2-charts'

import { PeopleService } from './people.service';

import { FilterPipe } from './filter.pipe';

const appRoutes: Routes = [
  { path: 'people', component: PeopleComponent },
  { path: 'person',  component: NewPersonComponent },
  { path: 'person/:id', component: PersonComponent },
  { path: 'peopleReport', component: PeopleReportComponent },
  { path: '', redirectTo: '/people',  pathMatch: 'full' },
  { path: '**', redirectTo: '/people',  pathMatch: 'full' }
];

@NgModule({
  declarations: [
    AppComponent,
    PeopleTableComponent,
    PersonComponent,
    NewPersonComponent,
    PeopleComponent,
    PeopleReportComponent,
    GenderBarChartComponent,
    AgeBarChartComponent,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    ChartsModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  providers: [
    PeopleService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
