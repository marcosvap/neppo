export class Person {
	public id: number;
    public nome: string;
    public dataNascimento: string;
    public identificacao: string;
    public sexo: string;
    public endereco: string;
}