import { Component, OnInit } from '@angular/core';
import { Router  , ActivatedRoute } from '@angular/router';
import { PeopleService } from '../people.service';
import { Person } from '../person';
import { Sexo } from '../sexo';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {

	person: Person;
	sexos = [
	    new Sexo("masc", "Masculino"),
	    new Sexo("fem", "Feminino"),
	];

	constructor(private activeRoute: ActivatedRoute, private peopleService: PeopleService, private router: Router) { 
		this.person = new Person();
	}

	ngOnInit() {

		this.peopleService.getPerson(this.activeRoute.snapshot.params['id']).subscribe(person => { 
        	this.person = person;
        });

	}

	onSubmit() { 
		this.peopleService.updatePerson(this.person).subscribe(() => {this.router.navigateByUrl('/people')}, error => {this.router.navigateByUrl('/people')});
   	}

}