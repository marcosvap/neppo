import { Component, OnInit } from '@angular/core';
import { PeopleService } from '../people.service';
import { Person } from '../person';
import {Router} from '@angular/router';
import { Sexo } from '../sexo';

@Component({
  selector: 'app-new-person',
  templateUrl: './new-person.component.html',
  styleUrls: ['./new-person.component.css']
})
export class NewPersonComponent implements OnInit {

	person: Person;
	sexos = [
	    new Sexo("masc", "Masculino"),
	    new Sexo("fem", "Feminino"),
	];

	constructor(private peopleService: PeopleService, private router: Router) {
		this.person = new Person();
	}

	ngOnInit() {
	}

	onSubmit() { 
	    this.peopleService.createPerson(this.person).subscribe(() => {this.router.navigateByUrl('/people')}, error => {this.router.navigateByUrl('/people')});
  	}

}