import { Component, OnInit } from '@angular/core';
import { PeopleService } from '../people.service';
import { Person } from '../person';

@Component({
  selector: 'app-age-bar-chart',
  templateUrl: './age-bar-chart.component.html',
  styleUrls: ['./age-bar-chart.component.css']
})
export class AgeBarChartComponent implements OnInit {

	ageLabels =  ['0 a 9', '10 a 19', '20 a 29', '30 a 39', 'Maior que 40'];
	people: Person[];
	ageLayer1: number;
	ageLayer2: number;
	ageLayer3: number;
	ageLayer4: number;
	ageLayer5: number;
	ageChartData: any[];
	ageReport = 'Relatorio de idades';
	chartOptions = {
		responsive: true
	};
	colors = [
	    {
	      backgroundColor: 'rgba(30, 169, 224, 0.8)'
	    }
	];

	constructor(private peopleService: PeopleService) {

		this.ageLayer1 = 0;
		this.ageLayer2 = 0;
		this.ageLayer3 = 0;
		this.ageLayer4 = 0;
		this.ageLayer5 = 0;

		this.peopleService.getPeople().subscribe(people => { 
			this.people = people; 

			var i;
			for(i = 0; i < this.people.length; i++){
				
				var date = new Date(this.people[i].dataNascimento);
				var today = new Date();

				var dateYear = date.getFullYear();
				var todayYear = today.getFullYear();

				var diff = todayYear - dateYear;

				switch (true) {
				    case diff < 10:
				        this.ageLayer1++;
				        break;
				    case diff < 20:
				        this.ageLayer2++;
				        break;
				    case diff < 30:
				        this.ageLayer3++;
				        break;
				    case diff < 40:
				        this.ageLayer4++;
				        break;
				    default:
				        this.ageLayer5++;
				        break;
				}

			}

			this.ageChartData = [
				{
					label: 'ages',
					data: [this.ageLayer1, this.ageLayer2, this.ageLayer3, this.ageLayer4, this.ageLayer5] 
				}
			];
		});

	}

	ngOnInit() {
	}

}