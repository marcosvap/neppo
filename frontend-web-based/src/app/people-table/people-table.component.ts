import { Component, OnInit } from '@angular/core';
import { PeopleService } from '../people.service';
import { Person } from '../person';

@Component({
  selector: 'app-people-table',
  templateUrl: './people-table.component.html',
  styleUrls: ['./people-table.component.css']
})
export class PeopleTableComponent implements OnInit {

	people: Person[];
	public searchString: string;

	constructor(private peopleService: PeopleService) { }

	ngOnInit() {

		this.peopleService.getPeople().subscribe(people => { 
            this.people = people;
        });

	}

	public deletePerson(personId) {
	    this.peopleService.deletePerson(personId).subscribe(() => {window.location.reload()}, error => window.location.reload());
  	}

}