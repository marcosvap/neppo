import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenderBarChartComponent } from './gender-bar-chart.component';

describe('GenderBarChartComponent', () => {
  let component: GenderBarChartComponent;
  let fixture: ComponentFixture<GenderBarChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenderBarChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenderBarChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
