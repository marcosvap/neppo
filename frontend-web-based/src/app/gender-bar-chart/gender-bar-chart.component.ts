import { Component, OnInit } from '@angular/core';
import { PeopleService } from '../people.service';
import { Person } from '../person';

@Component({
  selector: 'app-gender-bar-chart',
  templateUrl: './gender-bar-chart.component.html',
  styleUrls: ['./gender-bar-chart.component.css']
})
export class GenderBarChartComponent implements OnInit {

	people: Person[];
	genderMaleAmount: number;
	genderFemaleAmount: number;
	genderChartData: any[];
	genderReport = 'Relatorio de sexo';
	chartOptions = {
		responsive: true
	};
	colors = [
	    {
	      backgroundColor: 'rgba(30, 169, 224, 0.8)'
	    }
	];
	genderLabels =  ['Masculino', 'Feminino'];

	constructor(private peopleService: PeopleService) {
		this.genderMaleAmount = 0;
		this.genderFemaleAmount = 0;

		this.peopleService.getPeople().subscribe(people => { 
			this.people = people; 

			var i;
			for(i = 0; i < this.people.length; i++){
				if(this.people[i].sexo === "masc"){
					this.genderMaleAmount++;
				} else {
					this.genderFemaleAmount++;
				}
			}

			this.genderChartData = [
				{
					label: 'gender',
					data: [this.genderMaleAmount, this.genderFemaleAmount] 
				}
			];
		});
	}

	ngOnInit() {
	}

}
