package com.neppo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class NeppoApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(NeppoApiApplication.class, args);
	}
}
